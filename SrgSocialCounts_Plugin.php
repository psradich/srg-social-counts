<?php


include_once('SrgSocialCounts_LifeCycle.php');

class SrgSocialCounts_Plugin extends SrgSocialCounts_LifeCycle {

    /**
     * See: http://plugin.michael-simpson.com/?page_id=31
     * @return array of option meta data.
     */
    public function getOptionMetaData() {
        //  http://plugin.michael-simpson.com/?page_id=31
        return array(
            //'_version' => array('Installed Version'), // Leave this one commented-out. Uncomment to test upgrades.
            'facebookID' => array(__('Enter Facebook Page Name', 'srg-social-counts')),
            'twitterID' => array(__('Enter Twitter ID', 'srg-social-counts')),
        );
    }

//    protected function getOptionValueI18nString($optionValue) {
//        $i18nValue = parent::getOptionValueI18nString($optionValue);
//        return $i18nValue;
//    }

    protected function initOptions() {
        $options = $this->getOptionMetaData();
        if (!empty($options)) {
            foreach ($options as $key => $arr) {
                if (is_array($arr) && count($arr > 1)) {
                    $this->addOption($key, $arr[1]);
                }
            }
        }
    }

    public function getPluginDisplayName() {
        return 'SRG Social Counts';
    }

    protected function getMainPluginFileName() {
        return 'srg-social-counts.php';
    }

    /**
     * See: http://plugin.michael-simpson.com/?page_id=101
     * Called by install() to create any database tables if needed.
     * Best Practice:
     * (1) Prefix all table names with $wpdb->prefix
     * (2) make table names lower case only
     * @return void
     */
    protected function installDatabaseTables() {
        //        global $wpdb;
        //        $tableName = $this->prefixTableName('mytable');
        //        $wpdb->query("CREATE TABLE IF NOT EXISTS `$tableName` (
        //            `id` INTEGER NOT NULL");
    }

    /**
     * See: http://plugin.michael-simpson.com/?page_id=101
     * Drop plugin-created tables on uninstall.
     * @return void
     */
    protected function unInstallDatabaseTables() {
        //        global $wpdb;
        //        $tableName = $this->prefixTableName('mytable');
        //        $wpdb->query("DROP TABLE IF EXISTS `$tableName`");
    }


    /**
     * Perform actions when upgrading from version X to version Y
     * See: http://plugin.michael-simpson.com/?page_id=35
     * @return void
     */
    public function upgrade() {
    }

    public function addActionsAndFilters() {

        // Add options administration page
        // http://plugin.michael-simpson.com/?page_id=47
        add_action('admin_menu', array(&$this, 'addSettingsSubMenuPage'));

        // Example adding a script & style just for the options administration page
        // http://plugin.michael-simpson.com/?page_id=47
        //        if (strpos($_SERVER['REQUEST_URI'], $this->getSettingsSlug()) !== false) {
        //            wp_enqueue_script('my-script', plugins_url('/js/my-script.js', __FILE__));
        //            wp_enqueue_style('my-style', plugins_url('/css/my-style.css', __FILE__));
        //        }


        // Add Actions & Filters
        // http://plugin.michael-simpson.com/?page_id=37


        // Adding scripts & styles to all pages
        // Examples:
        //        wp_enqueue_script('jquery');
        //        wp_enqueue_style('my-style', plugins_url('/css/my-style.css', __FILE__));
        //        wp_enqueue_script('my-script', plugins_url('/js/my-script.js', __FILE__));


        // Register short codes
        add_shortcode('getFbCount', array($this, 'getFbCount'));
        add_shortcode('getTwitterCount', array($this, 'getTwitterFollowers'));
        // http://plugin.michael-simpson.com/?page_id=39


        // Register AJAX hooks
        // http://plugin.michael-simpson.com/?page_id=41

    }

    public function getFbCount(){
        $id =  $this->getOption('facebookID');
        $data = json_decode(file_get_contents('http://graph.facebook.com/'.$id.'?fields=likes'));
        $count = $data->likes;
        echo number_format($count);
    }
    
    function getTwitterFollowers()
    {
        $screenName = $this->getOption('twitterID');
        
        $settings = array(
        'oauth_access_token' => "35737324-AJXIbrRhAHtEOtthUmkPpMKDspQTGKhrvq49eEtVB",
        'oauth_access_token_secret' => "ESqPM73Dhbmz0qfHBC4CcBjOgKFMEPsVLKf5UeZoCjEIg",
        'consumer_key' => "83iLV6bMGjyKd5xP2Eoq5MEmT",
        'consumer_secret' => "B2qpGZLeCUxyKndtLvGpH1hlrLVWpcBQjtTuAKX0bOKLJGcSoK"
        );
        
        $ta_url = 'https://api.twitter.com/1.1/statuses/user_timeline.json';
        $getfield = '?screen_name='.$screenName;
        $requestMethod = 'GET';
        $twitter = new TwitterAPIExchange($settings);
        $follow_count=$twitter->setGetfield($getfield)
        ->buildOauth($ta_url, $requestMethod)
        ->performRequest();
        // print_r($follow_count);
        $data = json_decode($follow_count, true);
        $followers_count=$data[0]['user']['followers_count'];
        echo number_format($followers_count);
        
        // echo $screenName;
        // // some variables
        // $consumerKey = 'KzMdE7vdznCGNg1otYjeQ';
// $consumerSecret = '4B3WNR5tLpI1UAYezmx7ZD60BO2k5zvj6XNfPd7H0';
        // $token = get_option('cfTwitterToken');
//      
        // // get follower count from cache
        // $numberOfFollowers = get_transient('cfTwitterFollowers');
//      
        // // cache version does not exist or expired
        // if (false === $numberOfFollowers) {
            // // getting new auth bearer only if we don't have one
            // if(!$token) {
                // // preparing credentials
                // $credentials = $consumerKey . ':' . $consumerSecret;
                // $toSend = base64_encode($credentials);
//      
                // // http post arguments
                // $args = array(
                    // 'method' => 'POST',
                    // 'httpversion' => '1.1',
                    // 'blocking' => true,
                    // 'headers' => array(
                        // 'Authorization' => 'Basic ' . $toSend,
                        // 'Content-Type' => 'application/x-www-form-urlencoded;charset=UTF-8'
                    // ),
                    // 'body' => array( 'grant_type' => 'client_credentials' )
                // );
//      
                // add_filter('https_ssl_verify', '__return_false');
                // $response = wp_remote_post('https://api.twitter.com/oauth2/token', $args);
     // print_r($response);
                // $keys = json_decode(wp_remote_retrieve_body($response));
//      
                // if($keys) {
                    // // saving token to wp_options table
                    // update_option('cfTwitterToken', $keys->access_token);
                    // $token = $keys->access_token;
                // }
            // }
            // // we have bearer token wether we obtained it from API or from options
            // $args = array(
                // 'httpversion' => '1.1',
                // 'blocking' => true,
                // 'headers' => array(
                    // 'Authorization' => "Bearer $token"
                // )
            // );
//      
            // add_filter('https_ssl_verify', '__return_false');
            // $api_url = "https://api.twitter.com/1.1/users/show.json?screen_name=$screenName";
            // $response = wp_remote_get($api_url, $args);
//      
            // if (!is_wp_error($response)) {
                // $followers = json_decode(wp_remote_retrieve_body($response));
                // $numberOfFollowers = $followers->followers_count;
            // } else {
                // // get old value and break
                // $numberOfFollowers = get_option('cfNumberOfFollowers');
                // // uncomment below to debug
                // //die($response->get_error_message());
            // }
//      
            // // cache for an hour
            // set_transient('cfTwitterFollowers', $numberOfFollowers, 1*60*60);
            // update_option('cfNumberOfFollowers', $numberOfFollowers);
        // }
        // echo number_format($numberOfFollowers);
    }
}




/**
 * Twitter-API-PHP : Simple PHP wrapper for the v1.1 API
 * 
 * PHP version 5.3.10
 * 
 * @category Awesomeness
 * @package  Twitter-API-PHP
 * @author   James Mallison <me@j7mbo.co.uk>
 * @license  MIT License
 * @link     http://github.com/j7mbo/twitter-api-php
 */
class TwitterAPIExchange
{
    private $oauth_access_token;
    private $oauth_access_token_secret;
    private $consumer_key;
    private $consumer_secret;
    private $postfields;
    private $getfield;
    protected $oauth;
    public $url;

    /**
     * Create the API access object. Requires an array of settings::
     * oauth access token, oauth access token secret, consumer key, consumer secret
     * These are all available by creating your own application on dev.twitter.com
     * Requires the cURL library
     * 
     * @param array $settings
     */
    public function __construct(array $settings)
    {
        if (!in_array('curl', get_loaded_extensions())) 
        {
            throw new Exception('You need to install cURL, see: http://curl.haxx.se/docs/install.html');
        }
        
        if (!isset($settings['oauth_access_token'])
            || !isset($settings['oauth_access_token_secret'])
            || !isset($settings['consumer_key'])
            || !isset($settings['consumer_secret']))
        {
            throw new Exception('Make sure you are passing in the correct parameters');
        }

        $this->oauth_access_token = $settings['oauth_access_token'];
        $this->oauth_access_token_secret = $settings['oauth_access_token_secret'];
        $this->consumer_key = $settings['consumer_key'];
        $this->consumer_secret = $settings['consumer_secret'];
    }
    
    /**
     * Set postfields array, example: array('screen_name' => 'J7mbo')
     * 
     * @param array $array Array of parameters to send to API
     * 
     * @return TwitterAPIExchange Instance of self for method chaining
     */
    public function setPostfields(array $array)
    {
        if (!is_null($this->getGetfield())) 
        { 
            throw new Exception('You can only choose get OR post fields.'); 
        }
        
        if (isset($array['status']) && substr($array['status'], 0, 1) === '@')
        {
            $array['status'] = sprintf("\0%s", $array['status']);
        }
        
        $this->postfields = $array;
        
        return $this;
    }
    
    /**
     * Set getfield string, example: '?screen_name=J7mbo'
     * 
     * @param string $string Get key and value pairs as string
     * 
     * @return \TwitterAPIExchange Instance of self for method chaining
     */
    public function setGetfield($string)
    {
        if (!is_null($this->getPostfields())) 
        { 
            throw new Exception('You can only choose get OR post fields.'); 
        }
        
        $search = array('#', ',', '+', ':');
        $replace = array('%23', '%2C', '%2B', '%3A');
        $string = str_replace($search, $replace, $string);  
        
        $this->getfield = $string;
        
        return $this;
    }
    
    /**
     * Get getfield string (simple getter)
     * 
     * @return string $this->getfields
     */
    public function getGetfield()
    {
        return $this->getfield;
    }
    
    /**
     * Get postfields array (simple getter)
     * 
     * @return array $this->postfields
     */
    public function getPostfields()
    {
        return $this->postfields;
    }
    
    /**
     * Build the Oauth object using params set in construct and additionals
     * passed to this method. For v1.1, see: https://dev.twitter.com/docs/api/1.1
     * 
     * @param string $url The API url to use. Example: https://api.twitter.com/1.1/search/tweets.json
     * @param string $requestMethod Either POST or GET
     * @return \TwitterAPIExchange Instance of self for method chaining
     */
    public function buildOauth($url, $requestMethod)
    {
        if (!in_array(strtolower($requestMethod), array('post', 'get')))
        {
            throw new Exception('Request method must be either POST or GET');
        }
        
        $consumer_key = $this->consumer_key;
        $consumer_secret = $this->consumer_secret;
        $oauth_access_token = $this->oauth_access_token;
        $oauth_access_token_secret = $this->oauth_access_token_secret;
        
        $oauth = array( 
            'oauth_consumer_key' => $consumer_key,
            'oauth_nonce' => time(),
            'oauth_signature_method' => 'HMAC-SHA1',
            'oauth_token' => $oauth_access_token,
            'oauth_timestamp' => time(),
            'oauth_version' => '1.0'
        );
        
        $getfield = $this->getGetfield();
        
        if (!is_null($getfield))
        {
            $getfields = str_replace('?', '', explode('&', $getfield));
            foreach ($getfields as $g)
            {
                $split = explode('=', $g);
                $oauth[$split[0]] = $split[1];
            }
        }
        
        $base_info = $this->buildBaseString($url, $requestMethod, $oauth);
        $composite_key = rawurlencode($consumer_secret) . '&' . rawurlencode($oauth_access_token_secret);
        $oauth_signature = base64_encode(hash_hmac('sha1', $base_info, $composite_key, true));
        $oauth['oauth_signature'] = $oauth_signature;
        
        $this->url = $url;
        $this->oauth = $oauth;
        
        return $this;
    }
    
    /**
     * Perform the actual data retrieval from the API
     * 
     * @param boolean $return If true, returns data.
     * 
     * @return string json If $return param is true, returns json data.
     */
    public function performRequest($return = true)
    {
        if (!is_bool($return)) 
        { 
            throw new Exception('performRequest parameter must be true or false'); 
        }
        
        $header = array($this->buildAuthorizationHeader($this->oauth), 'Expect:');
        
        $getfield = $this->getGetfield();
        $postfields = $this->getPostfields();

        $options = array( 
            CURLOPT_HTTPHEADER => $header,
            CURLOPT_HEADER => false,
            CURLOPT_URL => $this->url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_TIMEOUT => 10,
        );

        if (!is_null($postfields))
        {
            $options[CURLOPT_POSTFIELDS] = $postfields;
        }
        else
        {
            if ($getfield !== '')
            {
                $options[CURLOPT_URL] .= $getfield;
            }
        }

        $feed = curl_init();
        curl_setopt_array($feed, $options);
        $json = curl_exec($feed);
        curl_close($feed);

        if ($return) { return $json; }
    }
    
    /**
     * Private method to generate the base string used by cURL
     * 
     * @param string $baseURI
     * @param string $method
     * @param array $params
     * 
     * @return string Built base string
     */
    private function buildBaseString($baseURI, $method, $params) 
    {
        $return = array();
        ksort($params);
        
        foreach($params as $key=>$value)
        {
            $return[] = "$key=" . $value;
        }
        
        return $method . "&" . rawurlencode($baseURI) . '&' . rawurlencode(implode('&', $return)); 
    }
    
    /**
     * Private method to generate authorization header used by cURL
     * 
     * @param array $oauth Array of oauth data generated by buildOauth()
     * 
     * @return string $return Header used by cURL for request
     */    
    private function buildAuthorizationHeader($oauth) 
    {
        $return = 'Authorization: OAuth ';
        $values = array();
        
        foreach($oauth as $key => $value)
        {
            $values[] = "$key=\"" . rawurlencode($value) . "\"";
        }
        
        $return .= implode(', ', $values);
        return $return;
    }

}

